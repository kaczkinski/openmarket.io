+++
author = "pbacksk"
comments = false
date = "2018-06-27"
draft = false
image = "../../static/images/HP-Compaq-8300.jpg"
menu = ""
share = false
slug = "HP ELITE 8300 SFF"
title = "Vente HP Compaq Elite 8300 SFF 4GO 1TO HDD"
+++

<img src="../../images/HP-Compaq-8300.jpg" id="HP8300" alt="HP8300" height="200" width="400">
```txt
Vente exceptionnelle d'un lot de postes informatiques HP Compaq Elite 8300 SFF avec licence Windows 7 Professionnel constructeur.
Les postes peuvent être achetés unitairement ou par lot(s).

*Tous les prix indiqués sont TTC

PRIX UNITAIRE: 189€

Pour toute information complémentaire ou demande d'achat, vous pouvez nous contacter à l'adresse de contact suivante:

contact.openmarketio@gmail.com

Les postes sont disponibles selon la configuration définie ci-dessous, néanmoins des options peuvent être définies à la commande.

Options disponibles:

- 8GO RAM [+45€] 
- Lecteur / Graveur DVD RW DRW [+30€]

Fiche technique du produit:

Modèle : HP Elite 8300
Format : SFF
Processeur : Intel Core i5-3470 3.20GHz - Turbo : 3.60 Ghz - 4 coeurs - DMI : 5 GT/s - Cache : 6 Mo - Socket FCLGA1155
Mémoire Vive : 4 Go - DDR3 / 32 Go maximum [VOIR OPTIONS]
Disque dur : 1 To HDD
Lecteur optique : Graveur DVD [VOIR OPTIONS]
Carte son : Intégrée
Réseau : Intel 82579LM GbE Gigabit Ethernet
Système d'exploitation installé : Microsoft Windows 7 Professionnel 64 bits (licence constructeur)
Alimentation : 240 Watts
Dimensions : Hauteur : 10 cm // Largeur : 33,8 cm // Profondeur : 37,8 cm

DÉTAILS DE LA CARTE GRAPHIQUE


PORTS
 
6 x Hi-Speed USB (2.0) (4 à l'avant, 2 à l'arrière)
4 x Super-Speed USB (3.0) (à l'arrière)
2 x PS/2 - Souris et Clavier
1 x Série RS-232 - D-Sub (DB-9) 9 broches
1 x VGA - HD D-Sub (HD-15) 15 broches
1 x DisplayPort
1 x Ethernet (RJ45) 10/100/1000Mbps
2 x Lignes d’entrée (stéréo / microphone)
2 x Lignes de sortie (casque / haut-parleur)
 
EMPLACEMENTS
 
4 x Slots - Mémoire vive
1 x PCI-Express (Low profile)
2 x PCI-Express x16 (Low profile)
1 x PCI (Low profile)
  
ACCESSOIRES LIVRÉS AVEC L'UNITÉ CENTRALE

1 x Clavier filaire
1 x Souris filaire
1 x Câble d'alimentation

GARANTIE
 
Produit garanti 6 mois
 
```
